<html>
<body>
<?php

ini_set( "display_errors", true );

require_once 'phar://glexchange.phar/GLExchange.php';
require_once 'phar://glexchange.phar/model/PDConfig.inc.php';
require_once 'phar://glexchange.phar/model/Document.inc.php';
require_once 'phar://glexchange.phar/model/Submission.inc.php';
require_once 'phar://glexchange.phar/model/CustomAttribute.inc.php';
require_once 'phar://glexchange.phar/model/ReferenceDocument.inc.php';

define ( 'URL', 'https://gl-sandbox2.translations.com/PD' );
define ( 'USERNAME', 'Peninsula_submitter' );
define ( 'PASSWORD', '28148050#' );
define ( 'RETRIEVER_USERNAME', 'Peninsula_submitter' );
define ( 'RETRIEVER_PASSWORD', '28148050#' );
define ( 'USERAGENT', 'ucf.php' );
define ( 'PROJECT', 'PEN000055' );
define ( 'SUBMISSION_NAME', 'PENINSULA_TEST_29MAY2017' );

echo "Starting<br>";
$connectionConfig = new PDConfig ();
$connectionConfig->url = URL;
$connectionConfig->username = USERNAME;
$connectionConfig->password = PASSWORD;
$connectionConfig->userAgent = USERAGENT;
$glxchange = new GLExchange ( $connectionConfig );
echo "Config initialized<br>";

$projects = $glxchange->getProjects ();
$project = $projects [0];

/*echo "Using project:" . $project->name . "<br>";
echo "Using project shortcide:" . $project->shortcode . "<br>";
echo "Using project ticket:" . $project->ticket . "<br>";*/

$submission = initSubmission ($project);
$glxchange->initSubmission ( $submission );
echo "Submission initialized<br>";

$document = new PDDocument ();
$document->fileformat = "XML";
$document->name = "test_php_document";
$document->sourceLanguage = "en-US";
$document->targetLanguages = array (
		"fr-FR", "zh-CN", "ar-QA", "pt-PT", "zh-TW", "es-ES", "ja-JP"
);
$document->data = LoadFile ( 'sample_hsh.xml' );
$glxchange->uploadTranslatable ( $document );
echo "Document '" . $document->name . "' submitted<br>";
$glxchange->startSubmission ();
echo "Submission started<br>";

//preliminary delivery
/*
$connectionConfig = new PDConfig ();
$connectionConfig->url = URL;
$connectionConfig->username = RETRIEVER_USERNAME;
$connectionConfig->password = RETRIEVER_PASSWORD;
$connectionConfig->userAgent = USERAGENT;
$glxchange = new GLExchange ( $connectionConfig );
claim ( $glxchange );
downloadTranslationKit ( $glxchange );
*/

//TESTS
/*echo "<br>Running tests<br>";
$glxchange = testConnectionConfig ();
$glxchange = testInitSubmission ( $project, $glxchange );
testUploadDocumentAndReference ( $project, $glxchange );*/

function testConnectionConfig() {
	echo "TESTING connectionConfig<br>";
	$connectionConfig = new PDConfig ();
	$connectionConfig->url = URL . "incorrect";
	$connectionConfig->username = USERNAME;
	$connectionConfig->password = PASSWORD;
	$connectionConfig->userAgent = USERAGENT;
	try {
		$glxchange = new GLExchange ( $connectionConfig );
		echo "<h3><h3>ERROR: Created with incorrect Url</h3>";
	} catch ( Exception $e ) {
		echo "OK: Expected exception catched. Url not correct<br>";
	}
	$connectionConfig->url = URL;
	$connectionConfig->username = USERNAME . "incorrect";
	try {
		$glxchange = new GLExchange ( $connectionConfig );
		echo "<h3>ERROR: Created with incorrect credentials</h3>";
	} catch ( Exception $e ) {
		echo "OK: Expected exception catched. Credentials not correct<br>";
	}
	$connectionConfig->username = USERNAME;
	$connectionConfig->userAgent = null;
	try {
		$glxchange = new GLExchange ( $connectionConfig );
		echo "<h3>ERROR: Created with empty useragent</h3>";
	} catch ( Exception $e ) {
		echo "OK: Expected exception catched. UserAgent not set<br>";
	}
	$connectionConfig->userAgent = USERAGENT;
	
	try {
		$glxchange = new GLExchange ( $connectionConfig );
		echo "OK: Created <br>";
	} catch ( Exception $e ) {
		echo "<h3>ERROR: Created with incorrect config</h3>";
	}
	echo "TEST finished<br><br>";
	return $glxchange;
}
function testInitSubmission($project, $glxchange) {
	echo "TESTING initSubmission<br>";
	$submission = new PDSubmission ();
	try {
		$glxchange->initSubmission ( $submission );
		echo "<h3>ERROR: Initialized empty submission</h3>";
	} catch ( Exception $e ) {
		echo "OK: Empty submission not initialized<br>";
	}
	
	$submission->project = $project;
	try {
		$glxchange->initSubmission ( $submission );
		echo "<h3>ERROR: Initialized unnamed submission</h3>";
	} catch ( Exception $e ) {
		echo "OK: Unnamed submission not initialized<br>";
	}
	
	$submission->name = SUBMISSION_NAME;
	$submission->project = NULL;
	try {
		$glxchange->initSubmission ( $submission );
		echo "<h3>ERROR: Initialized submission without project</h3>";
	} catch ( Exception $e ) {
		echo "OK: Submission without project not initialized<br>";
	}
	
	$submission->project = $project;
	$submission->submitter = "incorrect";
	try {
		$glxchange->initSubmission ( $submission );
		echo "<h3>ERROR: Initialized submission with incorrect submitter</h3>";
	} catch ( Exception $e ) {
		echo "OK: Submission with incorrect submitter not initialized<br>";
	}
	
	$submission->submitter = USERNAME;
	$submission->dueDate = 100;
	try {
		$glxchange->initSubmission ( $submission );
		echo "<h3>ERROR: Initialized submission with incorrect dueDate</h3>";
	} catch ( Exception $e ) {
		echo "OK: Submission with incorrect dueDate not initialized<br>";
	}
	
	$submission->dueDate = 1412561974714;
	try {
		$glxchange->initSubmission ( $submission );
		echo "OK: Initialized <br>";
	} catch ( Exception $e ) {
		echo "<h3>ERROR: Correct submission not initialized</h3>";
	}
	
	echo "TEST finished<br><br>";
	return $glxchange;
}
function testUploadDocumentAndReference($project, $glxchange) {
	$originalSubmission = initSubmission($project);
	$glxchange->initSubmission ( $originalSubmission );
	echo "TESTING uploadReference and uploadDocument<br>";
	$referenceDocument = new PDReferenceDocument ();
	try {
		$glxchange->uploadReference ( $referenceDocument );
		echo "<h3>ERROR: Uploaded empty reference document</h3>";
	} catch ( Exception $e ) {
		echo "OK: Empty reference document not uploaded<br>";
	}
	
	$referenceDocument->name = "test name";
	try {
		$glxchange->uploadReference ( $referenceDocument );
		echo "<h3>ERROR: Uploaded reference document without data</h3>";
	} catch ( Exception $e ) {
		echo "OK: Reference document without data not uploaded<br>";
	}
	
	$referenceDocument->name = NULL;
	$referenceDocument->data = "test data";
	try {
		$glxchange->uploadReference ( $referenceDocument );
		echo "<h3>ERROR: Uploaded reference document without name</h3>";
	} catch ( Exception $e ) {
		echo "OK: Reference document without name not uploaded<br>";
	}
	
	$referenceDocument->name = "test name";
	$referenceDocument->data = "test data";
	try {
		$glxchange->uploadReference ( $referenceDocument );
		echo "<h3>ERROR: Uploaded reference document before uploading translatable document</h3>";
	} catch ( Exception $e ) {
		echo "OK: Reference document not uploaded before uploading translatable document<br>";
	}
	
	try {
		$glxchange->startSubmission ();
		echo "<h3>ERROR: Started submission before uploading translatable document</h3>";
	} catch ( Exception $e ) {
		echo "OK: Submission not started before uploading translatable document<br>";
	}
	
	$document = new PDDocument ();
	try {
		$glxchange->uploadTranslatable ( $document );
		echo "<h3>ERROR: Uploaded empty translatable document</h3>";
	} catch ( Exception $e ) {
		echo "OK: Empty translatable document not uploaded<br>";
	}
	
	$document->data="test data";
	try {
		$glxchange->uploadTranslatable ( $document );
		echo "<h3>ERROR: Uploaded unnamed translatable document</h3>";
	} catch ( Exception $e ) {
		echo "OK: Unnamed translatable document not uploaded<br>";
	}
	
	$document->name = "test name";
	try {
		$glxchange->uploadTranslatable ( $document );
		echo "<h3>ERROR: Uploaded translatable document without fileFormat</h3>";
	} catch ( Exception $e ) {
		echo "OK: Translatable document without fileFormat not uploaded<br>";
	}
	
	$document->fileformat = "Incorrect";
	try {
		$glxchange->uploadTranslatable ( $document );
		echo "<h3>ERROR: Uploaded translatable document with incorrect fileFormat</h3>";
	} catch ( Exception $e ) {
		echo "OK: Translatable document with incorrect fileFormat not uploaded<br>";
	}
	
	$document->sourceLanguage = "en-US";
	try {
		$glxchange->uploadTranslatable ( $document );
		echo "<h3>ERROR: Uploaded translatable document without target languages</h3>";
	} catch ( Exception $e ) {
		echo "OK: Translatable document without target languages not uploaded<br>";
	}
	
	$document->targetLanguages = array (
			"aa-BB",
			"cc-DD" 
	);
	try {
		$glxchange->uploadTranslatable ( $document );
		echo "<h3>ERROR: Uploaded translatable document with incorrect language directions</h3>";
	} catch ( Exception $e ) {
		echo "OK: Translatable document with incorrect language directions not uploaded<br>";
	}
	$document = initDocument($project);
	$glxchange->uploadTranslatable($document);
	$submissionTicket = $glxchange->startSubmission();

	echo "<br>Testing started submission<br>";
	$pdSubmissionName = $glxchange->getSubmissionName($submissionTicket);
	if(!isset($pdSubmissionName)){
		echo "<h3>ERROR: Downloaded submission is null</h3>";
		return;
	} else {
		echo "OK: Submission exists<br>";
	}
	
	if($pdSubmissionName != SUBMISSION_NAME){
		echo "<h3>ERROR: Names not equal. Got '".pdSubmissionName."', but expected '".SUBMISSION_NAME."'</h3>";
	} else {
		echo "OK: Submission name is correct<br>";
	}
	echo "TEST finished<br><br>";
}

function claim($glxchange){

	$submissionWorkflowInfos = $glxchange->findAvailableWorkflowInfosForClaim(5);
	if(isset($submissionWorkflowInfos)){
		if(!is_array($submissionWorkflowInfos)){
			$tmp = $submissionWorkflowInfos;
			$submissionWorkflowInfos = array();
			array_push($submissionWorkflowInfos, $tmp);
		}
		foreach($submissionWorkflowInfos as &$submissionWorkflowInfo){
			$data = $glxchange->getPreliminaryTargets($submissionWorkflowInfo);
			file_put_contents('claimed.zip', $data);
		}
	}
}

function downloadTranslationKit($glxchange){
	
	$submissionWorkflowInfos = $glxchange->findAvailableWorkflowInfosForDownload(5);
	if(isset($submissionWorkflowInfos)){
		if(!is_array($submissionWorkflowInfos)){
			$tmp = $submissionWorkflowInfos;
			$submissionWorkflowInfos = array();
			array_push($submissionWorkflowInfos, $tmp);
		}
		foreach($submissionWorkflowInfos as &$submissionWorkflowInfo){
			$repositoryItems = $glxchange->downloadTranslationKit($submissionWorkflowInfo);
			foreach($repositoryItems as &$repositoryItem){
				file_put_contents('translationKit.zip', $data);
			}
		}
	}
}
function LoadFile($filename) {
	$fh = fopen ( $filename, 'rb' );
	
	$cache = '';
	$eof = false;
	
	$result = '';
	
	while ( 1 ) {
		
		if (! $eof) {
			if (! feof ( $fh )) {
				$row = fgets ( $fh, 4096 );
			} else {
				$eof = true;
			}
		}
		
		if ($eof)
			break;
		
		$result .= $row;
	}
	
	fclose ( $fh );
	
	return $result;
}
	function initDocument($project) {
		$document = new PDDocument();
		$document->data = "Some text to translate";
		$document->name = "Document name";
		$document->fileformat = $project->fileFormats[0];
		$document->sourceLanguage = $project->languageDirections[0]->sourceLanguage;
		$document->targetLanguages = array($project->languageDirections[0]->targetLanguage);
		return $document;
    }
    function initSubmission($project) {
    	$submission = new PDSubmission ();
		$submission->name = SUBMISSION_NAME;
		$submission->project = $project;
		$submission->submitter = USERNAME;
		$submission->isUrgent = true;
		$submission->pmNotes = "some pm notes";
		$submission->dueDate = 1412561974714;
		return $submission;
    }
?>
</body>
</html>
