<html>
<body>
<?php

define ( 'SOURCE_FOLDER', 'resources/' );
define ( 'TRANSLATED_FOLDER', 'translated/' );
if(!isset($_GET['submit']) && !isset($_GET['retrieve'])){
	echo "Please use 'usage.php?submit=true&retrieve=true' for submit and retrieve";
} else {
	require_once 'config.php';
	require_once 'phar://glexchange.phar/GLExchange.php';
	require_once 'phar://glexchange.phar/PDConfig.inc.php';
	require_once 'phar://glexchange.phar/Document.inc.php';
	require_once 'phar://glexchange.phar/Submission.inc.php';
	require_once 'phar://glexchange.phar/CustomAttribute.inc.php';
	require_once 'phar://glexchange.phar/ReferenceDocument.inc.php';

	if(isset($_GET['submit']) && $_GET['submit']==true){
		doSend();
	}
	if(isset($_GET['retrieve']) && $_GET['retrieve']==true){
		doRetrieve();
	}
}

function doSend(){
	global $sourceLanguage, $targetLanguages, $fileFormatProfile, $project;
	echo "Sending<br>";
	$glxchange = initGLExchange();
	echo "Config initialized<br>";

	
	if(!isset($sourceLanguage)){
		throw new Exception("Configuration option 'sourceLanguage' is not set");
	}
	
	if(!isset($targetLanguages)){
		throw new Exception("Configuration option 'targetLanguages' is not set");
	}
	
	if(!isset($fileFormatProfile)){
		throw new Exception("Configuration option 'fileFormatProfile' is not set");
	}
	
	if(!isset($project)){
		throw new Exception("Configuration option 'project' is not set");
	}

	$targetLangs = explode(",", $targetLanguages);
	
	if(count($targetLangs)<=0){
		throw new Exception("Configuration option 'targetLanguages' is not correct");
	}

	$pdproject = $glxchange->getProject($project);
	echo "Using project:" . $pdproject->name . "<br>";

	$submission = initSubmission ($pdproject);
	$glxchange->initSubmission ( $submission );
	echo "Submission initialized<br>";

	$dir    = SOURCE_FOLDER.$sourceLanguage;
	$files = scandir($dir);
	foreach ($files as &$file){
		if ($file != "." && $file != "..") {
			$document = new PDDocument ();
			$document->fileformat = $fileFormatProfile;
			$document->name = $file;
			$document->sourceLanguage = $sourceLanguage;
			$document->targetLanguages = $targetLangs;
			$document->data = LoadFile ( SOURCE_FOLDER.$sourceLanguage.'/'.$file );
			$glxchange->uploadTranslatable ( $document );
			echo "Document '" . $document->name . "' submitted<br>";
		}
	}
	
	$glxchange->startSubmission ();
	echo "Submission started<br><br>";
	
}

function doRetrieve(){
	echo "Retrieving<br>";
	$glxchange = initGLExchange();

	global $project;
	if(!isset($project)){
		throw new Exception("Configuration option 'project' is not set");
	}
	$pdproject = $glxchange->getProject($project);

	$targets = $glxchange->getCompletedTargetsByProject($pdproject, 9999);

	if(count($targets)>0){
		if (!file_exists(SOURCE_FOLDER.TRANSLATED_FOLDER)) {
			mkdir(SOURCE_FOLDER.TRANSLATED_FOLDER, 0777, true);
		}
		foreach ($targets as &$target) {
			try {
				$docTicket = $target->ticket;
				$translatedText = $glxchange->downloadTarget($docTicket);
				file_put_contents(SOURCE_FOLDER.TRANSLATED_FOLDER.$target->targetLocale."_".$target->documentName, $translatedText);
				//saveFile($target->documentName, $translatedText);
				// Do the processing that you need with the translated XML.
				echo $target->documentName . " [" . $docTicket . "] downloaded to ".SOURCE_FOLDER.TRANSLATED_FOLDER.$target->targetLocale."_".$target->documentName. "<br>";
				// On successful processing, send confirmation
				$glxchange->sendDownloadConfirmation($docTicket);
			} catch (Exception $e) {
				echo($e);
			}

		}
	} else {
		echo "No completed targets found<br>";
	}
	echo "Retrieving finished<br>";

		
}
function initGLExchange(){
	global $pdurl, $pdusername, $pdpassword, $userAgent;
	$connectionConfig = new PDConfig ();
	if(isset($pdurl)){
		$connectionConfig->url = $pdurl;
	} else {
		throw new Exception("Configuration option 'pdurl' is not set");
	}
	if(isset($pdusername)){
		$connectionConfig->username = $pdusername;
	} else {
		throw new Exception("Configuration option 'pdusername' is not set");
	}
	if(isset($pdpassword)){
		$connectionConfig->password = $pdpassword;
	} else {
		throw new Exception("Configuration option 'pdpassword' is not set");
	}
	if(isset($userAgent)){
		$connectionConfig->userAgent = $userAgent;
	} else {
		throw new Exception("Configuration option 'userAgent' is not set");
	}
	
	return new GLExchange ( $connectionConfig );
}

function LoadFile($filename) {
	$fh = fopen ( $filename, 'rb' );
	
	$cache = '';
	$eof = false;
	
	$result = '';
	
	while ( 1 ) {
		
		if (! $eof) {
			if (! feof ( $fh )) {
				$row = fgets ( $fh, 4096 );
			} else {
				$eof = true;
			}
		}
		
		if ($eof)
			break;
		
		$result .= $row;
	}
	
	fclose ( $fh );
	
	return $result;
}

    function initSubmission($project) {
    	$submission = new PDSubmission ();
		global $submissionPrefix;
		if(isset($submissionPrefix)){
			$submission->name = $submissionPrefix."glcapi.php sample";
		} else {
			$submission->name = "glcapi.php sample";
		}
		$submission->project = $project;
		$submission->isUrgent = true;
		$submission->pmNotes = "some pm notes";
		$submission->dueDate = strtotime('2017-09-01T22:35:17')*1000;
		return $submission;
    }
?>
</body>
</html>
