<?php include "php/html/header.php"?>
<?php $itemId= $_REQUEST["itemId"];?>
<?php $lang= $_REQUEST["lang"];?>
<div class="a4Container">
    <div id="header">
        <img id="pen_logo" src="images/peninsula.jpg" ?="">
    </div>
    <div id="content">

        <div class="container-fluid">

            <div class="thirdCol" id="itemTitle"></div>

        </div>
        <div class="container-fluid">
            <img id="itemImage">
        </div>
        <div class="container-fluid">
            <div id="des">
            </div>
        </div>
    </div>
    <div class="footer">The Peninsula Hong Kong,
        Salisbury Road, Kowloon, Hong Kong, SAR<br>
        Telephone: +852 2920 2888
        E-mail: phk@peninsula.com

    </div>
    <!--<div class="footer">The Peninsula Chicago,
        108 East Superior Street (at North Michigan Avenue), Chicago, Illinois 60611, USA<br>
        Toll-free: +1 312 337 2888
        E-mail: pch@peninsula.com

    </div>-->
</div>

<script>
    var App = {};
    $.ajax({
        url : "api/getItemList.php",
        method : "POST",
        dataType: "json",
        data : {itemId:"<?php echo($itemId);?>",
            lang:"<?php echo($lang);?>"}
    }).success(function(json){
        console.log("firstItemList = " + json);
        App.currentItem = json.data[0];
        console.log("App.currentItem.description_en = " + App.currentItem.description_en);
        $("#itemTitle").text( App.currentItem.title_en);

        $("#itemImage").attr("src", "upload/"+App.currentItem.fileName+".jpg");
        App.currentItem.description_en = App.currentItem.description_en.replace(/\n/g, "<br />");
        $("#des").html(App.currentItem.description_en);

    }).error(function(d){
        console.log('error');
        console.log(d);
    });

</script>