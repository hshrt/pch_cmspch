<div class='popup_box_container' id="translate_confirm">
        <div class='popup_box translate_confirm row'>
            <div class="col-sm-8">
                <h3 id="pageTitle">Submit Translate</h3>

                <h4 id="'title_title">Title</h4>
                    <input id="title" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

                <h4 id="'description_title">Description</h4>
                    <textarea id="description" rows="4" cols="50" class='form-control' name="description" ></textarea>

                <h4 id="'remark_title">Translation Remarks</h4>
                <div id="translation_remarks" class="translation_remarks">
                    <textarea id="remarks" rows="4" cols="50" class='form-control' name="remarks" ></textarea>
                </div>

                <h4 id="'file_title">Additional File</h4>
                <div id="additional_file" class="additional_file">
                    <form
                    <input id="add_file_field" class='form-control' type="file" >
                </div>
            </div>

            <div class="col-sm-4">
                <h4 id="'language_title">Translation To:</h4>
                <div id="translation_language" class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="zh-TW">Traditional Chinese
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="zh-CN">Simplified Chinese
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="fr-FR">French
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="pt-PT">Portuguese
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="es-ES">Spanish
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="ar-QA">Arabic
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="ja-JP">Japanese
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="ko-KR">Korean
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="de">German
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="ru">Russian
                    </label>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" checked="checked" id="tr">Turkish
                    </label>
                </div>
            </div>



            <div class='save_cancel_container col-sm-12'>
                <div class='round_btn btn bg-navy' id='cancel_btn'>Cancel</div>
                <div class='round_btn btn bg-olive' id='confirm_btn'>Submit</div>
                </div>
        </div>
</div>

