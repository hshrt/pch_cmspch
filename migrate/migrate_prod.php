
<?php

include("../../php/func_nx.php");
ini_set( "display_errors", true );

$type=isset($_REQUEST["type"])?$_REQUEST["type"]:"normal";

$servername = "localhost";
$username = "root";
$password = "ilikepch";

$database = "BSPPCH";

$records = array();
$dictionaries = array();
$dictionariesIds = array();
$mediaMaps = array();
$mediaIds = array();
$medias = array();

header('Content-Type: text/html; charset=utf-8');

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
mysqli_set_charset($conn,"utf8");

$itemId = isset($_REQUEST["itemId"])?$_REQUEST["itemId"]:null;

if($itemId != null){
	getRecord($itemId);
} else {
	getRecord("47bd8912-9126-11e6-ad79-e4115bbe5c3e"); // option sets
	getRecord("840e32f0-0e4b-11e5-9df3-984be11049af"); // items
}

switch ($type) {
	case "insert":
		foreach (array("items" => $records, "dictionary" => $dictionaries, "mediaItemMap" => $mediaMaps, "media" => $medias) as $table => $arr) {
			foreach ($arr as $r) {
				$tempValues = array_map(
					function($str) {
						$str = str_replace("\n","\\n", $str);
						return str_replace("'", "''", $str);
					},
					array_values($r)
				);
				$keys = "`" . implode("`,`", array_keys($r)) . "`";
				$values = "'" . implode("','", $tempValues) . "'";
				echoLine("INSERT INTO " . $GLOBALS["database"] ."." . $table . " (" . $keys . ") VALUES (" . $values . ");");
			}
		}
		break;
	default:
		$recordIds = array_map(function($o) {
			return $o["id"];
		}, $records);
		$dictionaryIds = array_map(function($o) {
			return $o["id"];
		}, $dictionaries);
		$mediaMapIds = array_unique(array_map(function($o) {
			return $o["itemId"];
		}, $mediaMaps));
		$mediaIds = array_map(function($o) {
			return $o["id"];
		}, $medias);
		echoLine("SELECT * FROM " . $GLOBALS["database"] . ".items WHERE `id` IN ('" . implode("','", $recordIds) . "');");
		echoLine("SELECT * FROM " . $GLOBALS["database"] . ".dictionary WHERE `id` IN ('" . implode("', '", $dictionaryIds) . "');");
		echoLine("SELECT * FROM " . $GLOBALS["database"] . ".mediaItemMap WHERE `itemId` IN ('" . implode("', '", $mediaMapIds) . "');");
		echoLine("SELECT * FROM " . $GLOBALS["database"] . ".media WHERE `id` IN ('" . implode("', '", $mediaIds) . "');");
		break;
}


$conn->close();

function echoLine ($text) {
	echo $text . "\n";
//	echo "<br>";
}

function getRecord($id) {
        $record = json_decode(getItem($id), true);
	if (!empty($record)) {
		$firstRecord = json_decode($record[0], true);
		$GLOBALS["records"][] = $firstRecord;

		getRecordDetail($firstRecord, 0);
	}
}

function getRecordDetail($record, $layer) {
	$recordDict = json_decode(getDict($record["titleId"]), true);
	if (!empty($recordDict)) {
		$firstDict = json_decode($recordDict[0], true);
		$GLOBALS["dictionaries"][] = $firstDict;
	}
	$dictionaryId = $record["descriptionId"];
	$descriptionDict = json_decode(getDict($dictionaryId), true);
	if (!empty($descriptionDict)) {
		$firstDescDict = json_decode($descriptionDict[0], true);
		if (!in_array($dictionaryId, $GLOBALS["dictionariesIds"])) {
			$GLOBALS["dictionariesIds"][] = $dictionaryId;
			$GLOBALS["dictionaries"][] = $firstDescDict;
		}
	}
    $recordMediaMap = json_decode(getMediaMap($record["id"]), true);

	foreach ($recordMediaMap as $v) {
		$mediaMap = json_decode($v, true);
		$GLOBALS["mediaMaps"][] = $mediaMap;

		$mediaId = $mediaMap["mediaId"];
		$media = json_decode(getMedia($mediaId), true);
		if (!empty($media)) {
			$firstMedia = json_decode($media[0], true);
			if (!in_array($mediaId, $GLOBALS["mediaIds"])) {
				$GLOBALS["mediaIds"][] = $mediaId;
				$GLOBALS["medias"][] = $firstMedia;
			}
		}
	}

	getChildRecords($record["id"], $layer);
}

function getChildRecords ($id, $layer) {
        $childRecords = json_decode(getChildItems($id), true);

	$childs = array();
	if (!empty($childRecords)) {
		foreach ($childRecords as $r) {
			$child = json_decode($r, true);
			$GLOBALS["records"][] = $child;
			$childs[] = getRecordDetail($child, $layer + 1);
		}
	}
}

function getDict ($id) {
        return sqlGet("SELECT * FROM " . $GLOBALS["database"] . ".dictionary where id like '$id'");
}

function getItem ($id) {
        return sqlGet("SELECT * FROM " . $GLOBALS["database"] . ".items where id like '$id' order by `order`");
}

function getChildItems ($id) {
        return sqlGet("SELECT * FROM " . $GLOBALS["database"] . ".items where parentId like '$id' and enable = 1 order by `order`");
}

function getMediaMap ($id) {
        return sqlGet("SELECT * FROM " . $GLOBALS["database"] . ".mediaItemMap where itemId like '$id'");
}

function getMedia ($id) {
        return sqlGet("SELECT * FROM " . $GLOBALS["database"] . ".media where id like '$id'");
}


function sqlGet ($sql) {
        global $conn;

        $result = $conn->query($sql);
        $data = array();
        if (!$result) {
                trigger_error('Invalid query: ' . $conn->error);
        } else if ($result->num_rows > 0) {
                // output data of each row
                while ( $row = $result->fetch_assoc() ){
                        $data[] = json_encode($row);
                }
                //echoLine("$result->num_rows results");
        } else {
                //echoLine("0 results");
        }

        return json_encode( $data );
}

exit;
