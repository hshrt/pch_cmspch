App.TranslatePopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self:null,
    dataObj:"",
    root:"",
    loading:false,
    isKey:false,
    langString:"",
    transType: "",
    remarks:"",

    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        this.self = this;
        if(options && options.isKey){
            this.isKey = options.isKey;
        }
        if(options && options.root){
            this.root = options.root;
        }
        if(options && options.dataObj){
            this.dataObj = options.dataObj;
            console.log("this keyObj = " + this.dataObj);
            console.log("keyObj = " + this.dataObj.key);
        }

        //lock the body scroll
        $('body').css({'overflow':'hidden'});
        $(document).bind('scroll',function () {

        });

        this.render();
    },
    events: {

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){

        var self = this;
        console.log("render translatePopup");
        $.ajax({
         url : "php/html/translatePopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             //console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){
                    $("#add_file_field").on('change', self.gotFile);
                    $('.popup_box_container').show(true);

                    $("#cancel_btn").on('click',function(){
                        self.destroy();
                    });
                    $("#confirm_btn").on('click',function(){
                        self.makeLangString();
                        self.remarks = $("#remarks").val();
                        self.processTranslation();
                    });

                });
            self.setupAllField();
         }).error(function(d){
            console.log('error');
            console.log(d);
         });


    },
    gotFile:function(event){
        if(event.target.files.length == 1) {
                var file = event.target.files[0];
                App.imageData = file;
                console.log(App.imageData.size);
                App.imageFileName = file.name;
                console.log(App.imageFileName);
        }
    },
    processTranslation:function(_id){
        var self = this;
        var formData = new FormData();
        formData.append("type", self.transType);
        console.log(App.imageData);
        if(self.transType=="key"){
            formData.append("key_eng", self.dataObj.en);
            formData.append("key_id", self.dataObj.id);
        }else{
            formData.append("title", self.title);
            formData.append("title_id", self.title_id);
            formData.append("description", self.description);
            formData.append("description_id", self.description_id);
        }
        formData.append("remarks", self.remarks);
        formData.append("fileData", App.imageData, App.imageFileName);
        formData.append("fileName", App.imageFileName);
        formData.append("targetLanguage", self.langString);

        $.ajax({
            url : "api/translation/submitTranslation.php",
            type : "POST",
            mimeType: "multipart/form-data",
            contentType: false,
            processData: false,
            data : formData
        }).success(function(json){
            console.log(json);
            console.log("msg = " + json.msg);

            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            App.translatePopup.destroy();

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    setupAllField: function(){

        console.log("keyObj = " + this.dataObj);
        $("#title").attr("readonly","readonly");
        $("#description").attr("readonly","readonly");
        if (this.isKey){
            console.log("isKey");
            $("#title").val(this.dataObj.key);
            $("#description").val(this.dataObj.en);
            this.transType = 'key';
        }else{
            //$(".title").val(this.dataObj.key);
            //$(".description").val(this.dataObj.en);
        }
    },


    close :function(){
        console.log("close fire");
    },
    getSelf: function(){
        return this.self;
    },
    destroy: function() {
        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        App.imageData = null;
        App.imageFileName = null;
        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow':'visible'});

    },
    makeLangString: function () {
        temp = [];
        $("#zh-TW").is(':checked') ? temp.push("zh-TW") : 0;
        $("#zh-CN").is(':checked') ? temp.push("zh-CN") : 0;
        $("#fr-FR").is(':checked') ? temp.push("fr-FR") : 0;
        $("#pt-PT").is(':checked') ? temp.push("pt-PT") : 0;
        $("#es-ES").is(':checked') ? temp.push("es-ES") : 0;
        $("#ar-QA").is(':checked') ? temp.push("ar-QA") : 0;
        $("#ja-JP").is(':checked') ? temp.push("ja-JP") : 0;
        $("#ko-KR").is(':checked') ? temp.push("ko-KR") : 0;
        $("#de").is(':checked') ? temp.push("de") : 0;
        $("#ru").is(':checked') ? temp.push("ru") : 0;
        $("#tr").is(':checked') ? temp.push("tr") : 0;
        this.langString = temp.toString();
    },
    isHide : false
});