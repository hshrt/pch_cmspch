App.MediaView = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    title: "Library",
    page:0,
    lock:false,//use to not duplicated click
    itemPerPage:20,
    initialize: function(options){
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },

    render: function(){
        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/mediaView.php",
            method : "POST",
            dataType: "html",
            data : {}
        }).success(function(html){

            $(self.el).append(html).
                promise()
                .done(function(){
                    self.loadPhoto(self.page);
                });


            //get album List for item
            self.getAlbumList();

            $("#mediaAlbumSelectionBox").change(function() {
                //alert( $("#albumSelectionBox").find(":selected").val() );
                console.log("Album changed");

                self.page = 0;
                self.loadPhoto(self.page);
                self.genPagination();

            });


            self.genPagination();


            $("#addBtn").on("click", function(){
                App.addPhotoPopup = new App.AddPhotoPopup(
                    {
                        root:self,
                        onlyShowUpLoad:true
                    }
                );
            });


        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },

    genPagination: function(){
        var self  = this;
        var albumSelection = $("#mediaAlbumSelectionBox").find(":selected").val();

        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {getCount: true,
                album:albumSelection,
                getAll:albumSelection==""?1:0

            }
        }).success(function(json){
            console.log("json = " + json);

            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }
            console.log("json = " + json.data[0].totalMediaNum);
            console.log("json = " + json.data);

            $("#paginationContainer").pagination({
                items: json.data[0].totalMediaNum,
                itemsOnPage: 20,
                cssStyle: 'light-theme',
                onPageClick:function(pageNum, event){
                    //alert("hi" + pageNum);
                    self.page = pageNum -1;
                    self.loadPhoto(pageNum-1);
                    //$("#paginationContainer").pagination('selectPage', pageNum);
                }
            });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    refresh: function(){
        var self = this;
        self.loadPhoto(self.page);
        //self.genPagination();
    },
    getAlbumList: function(){
        var self = this;
        var _url = "api/album/getAllAlbum.php";

        $.ajax({
            url : _url,
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){

            App.hideLoading();

            console.log(json);
            self.albumObjs = json.data;

            //add the online video item into the selector box
            for (var x = 0; x<self.albumObjs.length; x++) {
                var curAlbumObj = self.albumObjs[x];
                $("#mediaAlbumSelectionBox").append($('<option />').text(curAlbumObj.name).val(curAlbumObj.id));
            }


            console.log("jsonObj = " + self.albumObjs);

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });
    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    loadPhoto: function(page){
        $("#photoStreamContainer").empty();

        var self = this;

        var albumSelection = $("#mediaAlbumSelectionBox").find(":selected").val();

        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {page:page,
                    album:albumSelection,
                    getAll:albumSelection==""?1:0}
        }).success(function(json){
            console.log(json);
            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            var imageArray = json.data;

            console.log(imageArray);

            var container;
            //self.imageId = json.data.photoId;
            for(var x = 0;x<imageArray.length;x++){
                console.log(imageArray[x].image);
                var extension = imageArray[x].fileExt === "p" ? "png" : "jpg";
                var image =document.createElement('div');
                $(image).attr("order",x);
                console.log("image = " + imageArray[x].image);
                //image.src = "upload/"+imageArray[x].image+"_s.jpg";
                $(image).addClass("image fixHeight")

                try{
                    $(image).css({
                        "background-image": 'url('+"upload/"+imageArray[x].image+"_s."+extension+')'
                    });
                }
                catch(e){
                    console.log("error = " + e);
                    $(image).css({
                        "background-image": 'url('+"upload/"+imageArray[x].image+"_s"+extension+')'
                    });
                }

                container = $("#photoStreamContainer");

                container.append("<div id=" + "pop_image"+(x+page*20) + "></div>");

                $("#pop_image"+(x+page*20)).addClass("imageRoot");

                $("#pop_image"+(x+page*20)).attr("order",x);

                $("#pop_image"+(x+page*20)).css({"position":"relative","float":"left"});

                $("#pop_image"+(x+page*20)).append($(image));

                $("#pop_image"+(x+page*20)).on("click",function(){

                    console.log(App.mediaView.lock );
                    if(App.mediaView.lock ){
                        App.mediaView.lock = false;
                        return;
                    }
                    console.log("clicked");
                    var order = $(this).attr('order');
                    self.showAssignAlbumPopup(imageArray[order].image,imageArray[order].id,imageArray[order].album);

                });

                $("#pop_image"+(x+page*20)).append("<a class='removeImage' order=" + x + "></a>");

                $("#pop_image"+(x+page*20)).on("mouseover", function(){
                    $(this).find(".removeImage").show();
                });

                $("#pop_image"+(x+page*20)).on("mouseout", function(){
                    $(this).find(".removeImage").hide();
                });
            };

            //handle remove Image when click on remove button
            $(".removeImage").on("click",function(){
                App.mediaView.lock = true;
                console.log("delete position " +  $(this).attr('order'));

                var order = $(this).attr('order');
                App.yesNoPopup = new App.YesNoPopup(
                    {
                        yesFunc:function()
                        {

                            $.ajax({
                                url : "api/deleteItem.php",
                                method : "POST",
                                dataType: "json",
                                data : {id: imageArray[order].id, type:'media'}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                            }).success(function(json){

                                console.log(json);
                                //self.yesFunc();
                                App.yesNoPopup.destroy();

                                self.updateTotalMediaNum();

                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                            });
                        },
                        msg:"Are you sure to delete this photo?"
                    }
                );
            });

                $(".removeImage").on("mouseup",function() {

                        //App.mediaView.lock = true;

                }
                );

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    updateTotalMediaNum:function(){
        var self = this;

        var albumSelection = $("#mediaAlbumSelectionBox").find(":selected").val();

        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {getCount: true,
                album:albumSelection,
                getAll:albumSelection==""?1:0}
        }).success(function(json){
            console.log("json = " + json);
            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            console.log("json = " + json.data[0].totalMediaNum);
            console.log("json = " + json.data);

            $("#paginationContainer").pagination('updateItems', json.data[0].totalMediaNum);


            //fine tune the max page number
            var maxPage = 0;

            maxPage = json.data[0].totalMediaNum / self.itemPerPage;

            console.log("maxPage = " + maxPage);


            if(json.data[0].totalMediaNum % self.itemPerPage > 0){
                maxPage++;
            }

            console.log("maxPage = " + maxPage);
            console.log("self.page = " + self.page);

            if(self.page +1  > maxPage){

                self.page = maxPage - 1;
                console.log("changed page = " + self.page);

            }
            $("#paginationContainer").pagination('selectPage', self.page+1);

            //self.loadPhoto(self.page);

            /*$("#paginationContainer").pagination({
                items: json.data[0].totalMediaNum,
                itemsOnPage: 20,
                cssStyle: 'light-theme',
                onPageClick:function(pageNum, event){
                    //alert("hi" + pageNum);
                    self.page = pageNum -1;
                    self.loadPhoto(pageNum-1);
                    //$("#paginationContainer").pagination('selectPage', pageNum);
                }
            });*/

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function(){
        var that = this;
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: that.title == "Library"?"Page":that.title,
                parentId: that.parentId
            }
        );
    },

    showAssignAlbumPopup: function(_fileName,_mediaId,_album){
        var self = this;
        App.assignAlbumPopup = new App.AssignAlbumPopup(
            {
                fileName: _fileName,
                mediaId:_mediaId ,
                album:_album
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});