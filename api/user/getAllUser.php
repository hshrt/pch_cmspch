<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select * from user where email!='admin' order by firstname ASC";
$st = $conn->prepare ( $sql );


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'Get users ok!',$list);
else
    echo returnStatus(0 , 'Get users fail!');

$conn = null;

?>
