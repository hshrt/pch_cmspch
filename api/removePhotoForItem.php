<?php

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");

session_start();
include("checkSession.php");

$mediaId = $_POST['mediaId'];
$itemId= $_POST['itemId'];

if ( empty($mediaId)){
    echo returnStatus(0, 'missing_media id');
    exit;
}else{

    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $sql = "DELETE FROM mediaItemMap where mediaId = :mediaId and itemId = :itemId";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":mediaId", $mediaId, PDO::PARAM_STR);
    $st->bindValue( ":itemId", $itemId, PDO::PARAM_STR);
    $st->execute();

    $conn = null;

    echo returnStatus(1 , 'good');
}

?>
