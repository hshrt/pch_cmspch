<?php 

require("../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();
include("checkSession.php");

$title = isset($_REQUEST['title'])?$_REQUEST['title']:null;
$type = isset($_REQUEST['type'])?$_REQUEST['type']:null;

$skipCreateDescription = 0;

if(isset($_REQUEST['skipCreateDescription'])){
    $skipCreateDescription = $_REQUEST['skipCreateDescription'];
}

$parentId = 0;

if(isset($_REQUEST['parentId'])){
    $parentId = $_REQUEST['parentId'];
}

if(empty($title) || empty($type)){
    echo returnStatus(Invalid_input , "All field cannot be empty.");
    exit;
}

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

//print_r($st->errorInfo());

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid = $list[0]["UUID"];

$sql = "INSERT INTO dictionary (id,en,lastUpdate, lastUpdateBy) VALUES (:id,:title,now(),:email)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $uuid, PDO::PARAM_STR );
$st->bindValue( ":title", $title, PDO::PARAM_STR );
$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
$st->execute();

//print_r($st->errorInfo());


$titleId = $uuid;

$desId = 'descriptionIdTemp';

if(($type == "article" || $type == "item" || $type == "Spa/Restaurant" || $type == "Restaurant" || $type=="sub_item") && $skipCreateDescription == 0){

    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid_des = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary (id,en,lastUpdate, lastUpdateBy) VALUES (:id,:title,now(),:email)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_des, PDO::PARAM_STR );
    $st->bindValue( ":title", "", PDO::PARAM_STR );
    $st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
    $st->execute();
    $desId = $uuid_des;
    //echo("desId = ".$desId);
}

$sql = "select Max(items.order) as maxOrder from items where parentId = :parentId;";
$st = $conn->prepare ( $sql );
$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR );
$st->execute();

//print_r($st->errorInfo());

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$order = $list[0]["maxOrder"];

if($order == null){
    $order = 0;
}
else{
    $order++;
}


$sql = "INSERT INTO items (items.id,titleId, descriptionId, type, parentId,lastUpdate ,lastUpdateBy,items.order) VALUES (UUID
(),:titleId,:desId, :type, :parentId, CURRENT_TIMESTAMP,:email,:order)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":titleId", $titleId, PDO::PARAM_STR );
$st->bindValue( ":desId", $desId, PDO::PARAM_STR );
$st->bindValue( ":type", $type, PDO::PARAM_STR );
$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR );
$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
$st->bindValue( ":order",$order, PDO::PARAM_INT);
$st->execute();

//print_r($st->errorInfo());


//$this->id = $conn->lastInsertId();
$conn = null;
//echo $sql;

//this code print the error of running sql, useful
//echo $result->errorInfo();

//header( "Location: ../index.php" );

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, $titleId.",".$desId);
}
else{
    echo returnStatus(0, 'create item fail');
}
?>
