<?php

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");

require_once "../php/func_nx.php";
require_once "../php/func_json.php";

require("../php/lib/resize.class.php");

session_start();
//include("checkSession.php");

//pprint_r($_POST);

$photo_data = $_POST['image'];
$fileName = $_POST['fileName'];
$fileExtension = isset($_POST['fileExtension'])?$_POST['fileExtension']:"jpg";

if($fileExtension=="jpeg"){
    $fileExtension = "jpg";
}

$type = isset($_POST['type'])?$_POST['type']:null;
$album = isset($_POST['album'])?$_POST['album']:"";

if ( empty($photo_data)){
    echo returnStatus(0, 'missing_img_data');
    exit;
}else{

    $file_name_prefix =   '_' .time() . '_' . rand(10000,99999);
    $file_name_big = $fileName.$file_name_prefix . '.' .$fileExtension;
    $file_name_big_image = $file_name_big;

    $file_name_medium = $fileName.$file_name_prefix . '_m' .'.'. $fileExtension;
    $file_name_small = $fileName.$file_name_prefix . '_s'.'.'.$fileExtension;
    // save origin photo or pdf
    if ( file_put_contents('../upload/'.$file_name_big, base64_decode($photo_data)))
    {
        if($fileExtension=='pdf') {
                try {
                    $im = new Imagick();

                    $im->setResolution(100, 100);
                    $text_name = '../upload/'.$file_name_big.'[0]';
                    $im->readImage($text_name);
                    $im->setImageFormat('jpeg');
                    $file_name_big_image = $fileName.$file_name_prefix . '.' .'jpg';
                    $file_name_medium = $fileName.$file_name_prefix . '_m' .'.'.'jpg';
                    $file_name_small = $fileName.$file_name_prefix . '_s' .'.'.'jpg';
                    $im->writeImage('../upload/'.$file_name_big_image);
                    $im->clear();
                    $im->destroy();
                } catch(Exception $e) {
                    pprint_r($e);
                }

        }

	$error = "";

        $middle_size_width = 1024;
        $small_size_width = 300;

        if($type != null && $type == 'icon'){
            $middle_size_width = 100;
            $small_size_width = 50;
        }
        try{
            resizeSavePhoto('../upload/'.$file_name_big_image , $middle_size_width ,'../upload/'.$file_name_medium);
            resizeSavePhoto('../upload/'.$file_name_big_image , $small_size_width ,'../upload/'.$file_name_small);
        }
        catch(Exception $e){
            $error = $e;
            pprint_r($e);
        }

       /* // *** 1) Initialise / load image
        $resizeObj = new resize('../upload/'.$file_name_big);

        // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
        $resizeObj -> resizeImage(200, 200, 'exact');

        // *** 3) Save image
        $resizeObj -> saveImage($file_name_small, 100);*/


        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $conn->exec("set names utf8");

        $sql = "SELECT UUID() AS UUID";
        $st = $conn->prepare ( $sql );
        $st->execute();

        $list = array();

        while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
            $list[] = $row;
        }

        $uuid = $list[0]["UUID"];

        // todo
        // need to check here
        if($fileExtension == "pdf") {
            $extension = "pdf";
        } else {
            $extension = $fileExtension=="jpg"?"j":"p";
        }


        $sql = "INSERT INTO media (media.id,fileName,uploadTime,uploadBy,fileExt,album ) VALUES (:id,:fileName, now(),:email ,
:extension, :album)";
        $st = $conn->prepare ( $sql );

        $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
        $st->bindValue( ":fileName", $fileName.$file_name_prefix, PDO::PARAM_STR );
        $st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
        $st->bindValue( ":extension", $extension, PDO::PARAM_STR );
        $st->bindValue( ":album", $album, PDO::PARAM_STR );
        $st->execute();

        $photoId = $uuid;
    }

    echo returnStatus(1 , 'good', array('photoId' => $photoId, 'error' => $error));
}

function resizeSavePhoto($filename, $new_w, $out_filename){

    //echo "resize fire";
    $file = $filename;

    list($width , $height) = getimagesize($file);
    $ratio = $new_w / $width;
    //$new_w = $width * $ratio ;
    $new_h = $height * $ratio;

    $params = array(
        'constraint' => array('width' => $new_w, 'height' => $new_h)
    );
    img_resize($file, $out_filename, $params);
}


//echo  '{"'.'result'.'":'.json_encode($list).'}';


?>
