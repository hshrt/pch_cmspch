<?php
//define ( 'SOURCE_FOLDER', 'resources/' );
define ( 'SOURCE_FOLDER', './resources/' );
define ( 'TRANSLATED_FOLDER', 'translated/' );
define ( 'REFERENCE_FOLDER', './reference/' );

require_once 'config.php';
require_once 'GLExchange/GLExchange.php';
require_once 'GLExchange/model/PDConfig.inc.php';
require_once 'GLExchange/model/Document.inc.php';
require_once 'GLExchange/model/Submission.inc.php';
require_once 'GLExchange/model/CustomAttribute.inc.php';
require_once 'GLExchange/model/ReferenceDocument.inc.php';

$translationType = isset($_POST['type'])?$_POST['type']:null;
echo "type: ".$translationType."\n";
$translationTitle = isset($_POST['title'])?$_POST['title']:null;
$translationTitleId = isset($_POST['title_id'])?$_POST['title_id']:null;
$translationDescription = isset($_POST['description'])?$_POST['description']:null;
$translationDescriptionId = isset($_POST['description_id'])?$_POST['description_id']:null;
$translationKeyEng = isset($_POST['key_eng'])?$_POST['key_eng']:null;
$translationKeyId = isset($_POST['key_id'])?$_POST['key_id']:null;
$translationRemarks= isset($_POST['remarks'])?$_POST['remarks']:null;
$translationRemarksFile= isset($_FILES['fileData'])?$_FILES['fileData']:null;
$translationRemarksFileName= isset($_POST['fileName'])?$_POST['fileName']:null;
//$targetLanguages = isset($_POST['targetLanguage'])?$_POST['targetLanguage']:null;

var_dump($_POST);
var_dump($translationRemarksFile);
doSend();

Class SimpleXMLElementExtended extends SimpleXMLElement {

    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL) {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}

function createXML()
{
    global $translationType, $translationKeyEng,$translationKeyId,$translationTitleId,$translationTitle,$translationDescription,$translationDescriptionId,$translationRemarks;
    $newXML = new SimpleXMLElementExtended("<content-export></content-export>");
    //--meta--
    $metaXML = $newXML->addChild("meta");
    $metaXML->addChild("title");
    $metaXML->addChild("submitter");
    //--item--
    $itemXML = $newXML->addChild("item");
    $itemXML->addAttribute('type', $translationType);

    if ($translationType == "key") {
        $keyXML = $newXML->addChildWithCDATA("attribute", $translationKeyEng);
        $keyXML->addAttribute("dictionary-id", $translationKeyId);
        $keyXML->addAttribute("data-type", "key");
    }else{
        $titleXML = $newXML->addChildWithCDATA("attribute",$translationTitle);
        $titleXML->addAttribute("dictionary-id", $translationTitleId);
        $titleXML->addAttribute("data-type", "title");
        $descriptionXML = $newXML->addChildWithCDATA("attribute",$translationDescription);
        $descriptionXML->addAttribute("dictionary-id", $translationDescriptionId);
        $descriptionXML->addAttribute("data-type", "description");
    }
    $remarksXML = $newXML->addChildWithCDATA("attribute",$translationRemarks);
    $remarksXML->addAttribute("data-type", "remarks");

    echo $newXML->asXML();
    return $newXML->asXML();
}

function doSend(){
    global $sourceLanguage, $targetLanguages, $fileFormatProfile, $project, $translationRemarksFile, $translationRemarksFileName;
    echo "Sending...\n";
    $glxchange = initGLExchange();
    echo "Config initialized...\n";


    if(!isset($sourceLanguage)){
        throw new Exception("Configuration option 'sourceLanguage' is not set");
    }

    if(!isset($targetLanguages)){
        throw new Exception("Configuration option 'targetLanguages' is not set");
    }

    if(!isset($fileFormatProfile)){
        throw new Exception("Configuration option 'fileFormatProfile' is not set");
    }

    if(!isset($project)){
        throw new Exception("Configuration option 'project' is not set");
    }

    $targetLangs = explode(",", $targetLanguages);

    if(count($targetLangs)<=0){
        throw new Exception("Configuration option 'targetLanguages' is not correct");
    }

    $pdproject = $glxchange->getProject($project);
    echo "Using project: " . $pdproject->name . "\nProject shortcode is: " . $project . "\n";

    $submission = initSubmission ($pdproject);
    $glxchange->initSubmission ( $submission );
    echo "Submission initialized...\n";

    $file = createXML();
    // Source doucment(s)
    if ($file != "." && $file != "..") {
        $document = new PDDocument ();
        $document->fileformat = $fileFormatProfile;
        $document->name = "test_file.xml";
        $document->sourceLanguage = $sourceLanguage;
        $document->targetLanguages = $targetLangs;
        $document->data = $file;
        $ft = $glxchange->uploadTranslatable ( $document );
            echo "Document '" . $document->name . "' submitted, documentTicket is: " . $ft . "\n";
        }
    // Reference Document(s)
    $filesRef = $translationRemarksFile;
    echo "translationRemarksFileName: ".$translationRemarksFileName."\n";
   if ($filesRef!=null && $filesRef != "." && $filesRef != "..") {
            echo $translationRemarksFileName;
            $referenceDocument = new PDReferenceDocument ();
            $referenceDocument->name = $translationRemarksFileName;
            $referenceDocument->data = LoadFile ( $translationRemarksFile['tmp_name'] );
            $rft = $glxchange->uploadReference ( $referenceDocument );
            echo "Reference Document '" . $referenceDocument->name . "' submitted, reference document UID is: " . $rft . "\n";
        }
    // Start the submission
    $st = $glxchange->startSubmission ();
    echo "Submission started and submissionTicket is: " .$st . "\n";

}


function initGLExchange(){
    global $pdurl, $pdusername, $pdpassword, $userAgent;
    $connectionConfig = new PDConfig ();
    if(isset($pdurl)){
        $connectionConfig->url = $pdurl;
    } else {
        throw new Exception("Configuration option 'pdurl' is not set");
    }
    if(isset($pdusername)){
        $connectionConfig->username = $pdusername;
    } else {
        throw new Exception("Configuration option 'pdusername' is not set");
    }
    if(isset($pdpassword)){
        $connectionConfig->password = $pdpassword;
    } else {
        throw new Exception("Configuration option 'pdpassword' is not set");
    }
    if(isset($userAgent)){
        $connectionConfig->userAgent = $userAgent;
    } else {
        throw new Exception("Configuration option 'userAgent' is not set");
    }

    return new GLExchange ( $connectionConfig );
}

function LoadFile($filename) {
    $fh = fopen ( $filename, 'rb' ) or die("Can't open file: ".$filename."\n");
    $cache = '';
    $eof = false;

    $result = '';

    while ( 1 ) {

        if (! $eof) {
            if (! feof ( $fh )) {
                $row = fgets ( $fh, 4096 );
            } else {
                $eof = true;
            }
        }

        if ($eof)
            break;

        $result .= $row;
    }

    fclose ( $fh );

    return $result;
}

function initSubmission($project) {
    $submission = new PDSubmission ();
    global $submissionPrefix;
    if(isset($submissionPrefix)){
        $submission->name = $submissionPrefix."glcapi.php sample";
    } else {
        $submission->name = "glcapi.php sample";
    }
    $submission->project = $project;
    $submission->isUrgent = true;
    $submission->pmNotes = "Sample test notes, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    //Setting a timezone to avoid PHP warnings
    date_default_timezone_set("America/New_York");
    //604800 is the amount of seconds on a week, we multiply by 1000 because PD is expecting miliseconds
    $submission->dueDate = (time()+604800)*1000;
    return $submission;
}
?>
