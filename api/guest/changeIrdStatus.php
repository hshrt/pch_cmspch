<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$room = isset($_POST['room'])?$_POST['room']:null;
$ird = isset($_POST['ird'])?$_POST['ird']:null;

if ( empty($room)){
    echo returnStatus(0, 'missing room number');
    exit;
}

if ( empty($ird)) {
    echo returnStatus(0, 'missing ird status');
    exit;
}


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE allroom SET enable_ird = :ird where room = :room";
$st = $conn->prepare ( $sql );

$st->bindValue( ":room", $room, PDO::PARAM_STR );
$st->bindValue( ":ird", $ird, PDO::PARAM_STR );


$st->execute();

echo returnStatus(1, 'update ok');

$conn = null;

?>
