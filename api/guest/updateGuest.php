<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$salutation = isset($_POST['salutation'])?$_POST['salutation']:null;
$firstName = isset($_POST['firstName'])?$_POST['firstName']:null;
$lastName = isset($_POST['lastName'])?$_POST['lastName']:null;
$room = isset($_POST['room'])?$_POST['room']:null;
$checkIn = isset($_POST['checkIn'])?$_POST['checkIn']:null;
$checkOut = isset($_POST['checkOut'])?$_POST['checkOut']:null;
$reserveId = isset($_POST['reserveId'])?$_POST['reserveId']:null;
$memberId = isset($_POST['memberId'])?$_POST['memberId']:null;
$guestId = isset($_POST['guestId'])?$_POST['guestId']:null;

if ( empty($guestId)){
    echo returnStatus(0, 'missing guest Id');
    exit;
}


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE guest SET salutation=:salutation,firstName=:firstName,lastName=:lastName,room=:room, checkIn=:checkIn, checkOut=:checkOut,reserveId=:reserveId, memberId=:memberId where id = :guestId";
$st = $conn->prepare ( $sql );

$st->bindValue( ":salutation", $salutation, PDO::PARAM_STR );
$st->bindValue( ":firstName", $firstName, PDO::PARAM_STR );
$st->bindValue( ":lastName", $lastName, PDO::PARAM_STR );
$st->bindValue( ":room", $room, PDO::PARAM_STR );
$st->bindValue( ":checkIn", $checkIn, PDO::PARAM_STR );
$st->bindValue( ":checkOut", $checkOut, PDO::PARAM_STR );
$st->bindValue( ":reserveId", $reserveId, PDO::PARAM_STR );
$st->bindValue( ":memberId", $memberId, PDO::PARAM_STR );
$st->bindValue( ":guestId", $guestId, PDO::PARAM_STR );


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'update ok!');
else
    echo returnStatus(0 , 'update fail! May be there is no change?');

$conn = null;

?>
