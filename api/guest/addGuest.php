<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//session_start();

$roomNum = isset($_POST['roomNum'])?$_POST['roomNum']:'';
$regNum = isset($_POST['regNum'])?$_POST['regNum']:"";
$lang= isset($_POST['lang'])?$_POST['lang']:'';
$title= isset($_POST['title'])?$_POST['title']:'';
$firstName = isset($_POST['firstName'])?$_POST['firstName']:'';
$lastName = isset($_POST['lastName'])?$_POST['lastName']:'';
$arriveDate = isset($_POST['arriveDate'])?$_POST['arriveDate']:'';
$departureDate = isset($_POST['departureDate'])?$_POST['departureDate']:'';

$lastUpdateBy = isset($_POST['lastUpdateBy'])?$_POST['lastUpdateBy']:'';


if ( empty($roomNum)){
    echo returnStatus(0, 'missing roomNum');
    exit;
}
else{
    if(isset($_POST['roomNum'])){
        if(isset($_POST['update'])){

            //echo("update!!");

            $updateResult = updateGuest($roomNum,$regNum,$title,$lastName,$arriveDate,
                $departureDate,$lastUpdateBy);

            if($updateResult == 0)
                echo returnStatus(0 , 'cannot update');
            else
                echo returnStatus(1 , 'good');
        }
        else{
            $insertResult = insertIntoGuest($roomNum,$regNum,$lang,$title,$firstName,$lastName,$arriveDate,
                $departureDate,$lastUpdateBy);

           // echo($insertResult. " = InsertResult");
            if($insertResult == 0)
                echo returnStatus(0 , 'cannot create new guest');
            else
                echo returnStatus(1 , 'good');
        }
    }
}

function insertIntoGuest($roomNum,$regNum,$lang,$title,$firstName = null,$lastName,$arriveDate,$departureDate,
                         $lastUpdateBy){
    //echo "insertIntoGuest ";
    if ( empty($roomNum)){
        //echo returnStatus(0, 'missing_media id');
        return 0;
    }
    else{
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $conn->exec("set names utf8");

        $sql = "SELECT UUID() AS UUID";
        $st = $conn->prepare ( $sql );
        $st->execute();

        $list = array();

        while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
            $list[] = $row;
        }

        $uuid_des = $list[0]["UUID"];

        try {
            $sql = "INSERT INTO guest (id,room,regNum,lang,salutation,firstName,lastName,checkIn,checkOut,status,lastUpdate,lastUpdateBy) VALUES (:id,:room,:regNum,:lang,:salutation,:firstName,:lastName,:checkIn,:checkOut,:status,now(),:lastUpdateBy)";
            $st = $conn->prepare ( $sql );
            $st->bindValue( ":id", $uuid_des, PDO::PARAM_STR );
            $st->bindValue( ":room", $roomNum, PDO::PARAM_STR );
            $st->bindValue( ":regNum", $regNum, PDO::PARAM_STR );
            $st->bindValue( ":lang", $lang, PDO::PARAM_STR );
            $st->bindValue( ":salutation", $title, PDO::PARAM_STR );
            $st->bindValue( ":firstName", $firstName, PDO::PARAM_STR );
            $st->bindValue( ":lastName", $lastName, PDO::PARAM_STR );
            $st->bindValue( ":checkIn", $arriveDate, PDO::PARAM_STR );
            $st->bindValue( ":checkOut", $departureDate, PDO::PARAM_STR );
            $st->bindValue( ":status", "in", PDO::PARAM_STR );
            $st->bindValue( ":lastUpdateBy",$lastUpdateBy , PDO::PARAM_STR );
            //$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
            $st->execute();

            //echo("sql = ". $sql."</br>");

            if($st->rowCount() > 0){
                return 1;
            }
            else{
                return 0;
            }
        }
        catch(Exception $e) {
            //echo 'Exception -> ';
            var_dump($e->getMessage());
        }


        $conn = null;
        //$desId = $uuid_des;
    }
    return 0;

}

function updateGuest($roomNum,$regNum,$title,$lastName,$arriveDate,$departureDate,$lastUpdateBy){
    if ( empty($roomNum)){
        //echo ' miss room num!!!!!';
        return 0;
    }
    else{
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $conn->exec("set names utf8");

        $sql = "SELECT UUID() AS UUID";
        $st = $conn->prepare ( $sql );
        $st->execute();

        $list = array();

        while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
            $list[] = $row;
        }

        $uuid_des = $list[0]["UUID"];

        $sql = "UPDATE guest SET salutation=:salutation,lastName=:lastName,
        checkIn=:checkIn, checkOut=:checkOut, lastUpdate=now(),lastUpdateBy=:lastUpdateBy where room = :room";

        //echo "Sql = ".$sql;

        echo "title = ". $title."</br>";
        echo "lastName = ". $lastName."</br>";
        echo "arriveDate = ". $arriveDate."</br>";
        echo "departureDate = ". $departureDate."</br>";
        echo "lastUpdateBy = ". $lastUpdateBy."</br>";
        echo "room = ". $roomNum."</br>";
        echo "regNum = ". $regNum."</br>";

        $st = $conn->prepare ( $sql );
        $st->bindValue( ":room", $roomNum, PDO::PARAM_STR );
        //$st->bindValue( ":regNum", $regNum, PDO::PARAM_STR );
        $st->bindValue( ":salutation", $title, PDO::PARAM_STR );
        $st->bindValue( ":lastName", $lastName, PDO::PARAM_STR );
        $st->bindValue( ":checkIn", $arriveDate, PDO::PARAM_STR );
        $st->bindValue( ":checkOut", $departureDate, PDO::PARAM_STR );
        $st->bindValue( ":lastUpdateBy", $lastUpdateBy, PDO::PARAM_STR );
        //$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
        $st->execute();

        if($st->fetchColumn() > 0 || $st->rowCount() > 0){
            return 1;
        }
        else{
            return 0;
        }

        $conn = null;
        //$desId = $uuid_des;
    }

    return 0;
}

?>
