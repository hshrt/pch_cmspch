<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");


$room = isset($_REQUEST['room'])?$_REQUEST['room']:'';


if ( empty($room)){
    echo returnStatus(0, 'missing room number');
    exit;
}
else{

    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $sql = "select * from message where boardcast = 2 AND NOW() >= startDate AND NOW() <= endDate AND status!='D'";

    $st = $conn->prepare ( $sql );

    $st->execute();

    $msglist = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $msglist[] = $row;
    }
    //pprint_r($msglist);
    for($x=0;$x<sizeof($msglist);$x++){
        $sql = "insert into roomMessageMap(room, messageId,roomMessageMap.read, lastUpdate, lastUpdateby ) VALUES (:room,:msgId,0,NOW(),
:lastUpdateBy)";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":room", $room+"", PDO::PARAM_STR);
        $st->bindValue( ":msgId", $msglist[$x]['id'], PDO::PARAM_STR);
        $st->bindValue( ":lastUpdateBy", "system", PDO::PARAM_STR);
        $st->execute();
    }

    $conn = null;

    echo returnStatus(1 , 'checkin ok!');

}


?>
