<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$irdStatus = isset($_POST['irdStatus'])?$_POST['irdStatus']:null;


if ( empty($irdStatus)) {
    echo returnStatus(0, 'missing ird status');
    exit;
}


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE allroom SET enable_ird = :irdStatus";
$st = $conn->prepare ( $sql );

$st->bindValue( ":irdStatus", $irdStatus, PDO::PARAM_STR );


$st->execute();

echo returnStatus(1, 'update all ok');

$conn = null;

?>
