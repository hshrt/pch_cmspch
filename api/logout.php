<?php
session_start();

ini_set( "display_errors", true );
require( "../config.php" );
require("../php/inc.appvars.php");

unset($_SESSION['email']);
session_destroy();

echo returnStatus(1 , 'Logout');
?>