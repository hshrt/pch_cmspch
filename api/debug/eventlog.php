<?php
/**
 * Created by PhpStorm.
 * User: terenceho
 * Date: 22/1/2018
 * Time: 9:29
 */

require( "../../config.php" );
require( "../../php/func_nx.php");
require("../../php/inc.appvars.php");

$date = isset($_REQUEST['date'])?$_REQUEST['date']:null;
$roomid = isset($_REQUEST['roomId'])?$_REQUEST['roomId']:null;
$event = isset($_REQUEST['event'])?$_REQUEST['event']:null;
$string = isset($_REQUEST['string'])?$_REQUEST['string']:null;

//setup DB
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "INSERT into debug_eventlog (roomID,eventDate, event, eventString, lastUpdate) VALUES (:roomID, :eventDate, :event,:eventString, now())";

$st = $conn->prepare ($sql);

$st->bindValue( ":roomID", $roomid, PDO::PARAM_STR );
$st->bindValue( ":eventDate", $date, PDO::PARAM_STR );
$st->bindValue( ":event", $event, PDO::PARAM_STR );
$st->bindValue( ":eventString", $string, PDO::PARAM_STR );
$st->execute();

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'Insert record good');
}
else{
    echo returnStatus(1 , 'Insert record fail');
}


$conn = null;

