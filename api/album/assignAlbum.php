<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$mediaId = isset($_REQUEST['mediaId'])?$_REQUEST['mediaId']:null;
$album = isset($_REQUEST['album'])?$_REQUEST['album']:"";

if ( empty($mediaId) && empty($album)){
    echo returnStatus(0, 'missing mediaId or album ');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "UPDATE media SET album=:album where id = :mediaId";


$st = $conn->prepare ( $sql );

$st->bindValue( ":album", $album, PDO::PARAM_STR );
 $st->bindValue( ":mediaId", $mediaId, PDO::PARAM_STR);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'assign album to photo ok!');
else
    echo returnStatus(0 , 'assign album to photo fail!');

$conn = null;

?>
